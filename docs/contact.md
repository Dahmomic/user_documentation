# Contact Us
ME1100 V2 : Getting started - Application installation and performance benchmarking  

1.  [ME1100 V2](index.html)
2.  [User Documentation - ME1100](User-Documentation---ME1100_786039735.html)
3.  [Getting started](Getting-started_809697581.html)

ME1100 V2 : Getting started - Application installation and performance benchmarking
===================================================================================

Created by Rémi Paquette, last modified by Maude Maher-Bussieres on Oct 10, 2019

_______{This article provides step-by-step instructions to get a customer application installed for the first time in a lab environment and to get ready for application performance benchmarking.}_______

[![](https://kontron.atlassian.net/secure/viewavatar?size=medium&avatarId=19040&avatarType=issuetype)ME1100V2-462](https://kontron.atlassian.net/browse/ME1100V2-462) - Getting Started - Application installation & performance assessment Awaiting Publish 

* * *

**Table of contents**

/\*<!\[CDATA\[\*/ div.rbtoc1571313641038 {padding: 0px;} div.rbtoc1571313641038 ul {list-style: disc;margin-left: 0px;} div.rbtoc1571313641038 li {margin-left: 0px;padding-left: 0px;} /\*\]\]>\*/

*   [Introduction](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Introduction)
    *   [Assumptions](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Assumptions)
*   [Unboxing the platform](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Unboxingtheplatform)
    *   [What's in the box](#Gettingstarted-Applicationinstallationandperformancebenchmarking-What'sinthebox)
*   [Planning](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Planning)
    *   [Material and information required](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Materialandinformationrequired)
        *   [PCIe add-in card](#Gettingstarted-Applicationinstallationandperformancebenchmarking-PCIeadd-incard)
        *   [Power cables and tooling](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Powercablesandtooling)
        *   [Rack installation material](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Rackinstallationmaterial)
        *   [Network cables and modules](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Networkcablesandmodules)
        *   [Network infrastructure](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Networkinfrastructure)
    *   [Software required](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Softwarerequired)
*   [Installing a PCIe add-in card in an ME1100](#Gettingstarted-Applicationinstallationandperformancebenchmarking-InstallingaPCIeadd-incardinanME1100)
    *   [Opening the enclosure](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Openingtheenclosure)
    *   [Adjusting the PCIe add-in card space length to three-quarter length](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AdjustingthePCIeadd-incardspacelengthtothree-quarterlength)
    *   [Adjusting the PCIe add-in card rear mounting bracket](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AdjustingthePCIeadd-incardrearmountingbracket)
    *   [Connecting the PCIe add-in card](#Gettingstarted-Applicationinstallationandperformancebenchmarking-ConnectingthePCIeadd-incard)
    *   [Installing a thermal probe for the PCIe add-in card](#Gettingstarted-Applicationinstallationandperformancebenchmarking-InstallingathermalprobeforthePCIeadd-incard)
        *   [Locating the thermal probe connection](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Locatingthethermalprobeconnection)
        *   [Installing the thermal probe](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Installingthethermalprobe)
    *   [Closing the enclosure](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Closingtheenclosure)
*   [Racking the platform](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Rackingtheplatform)
*   [Connecting the network cables](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Connectingthenetworkcables)
*   [Building and connecting the power cables](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Buildingandconnectingthepowercables)
    *   [Material required](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Materialrequired)
    *   [Procedure](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Procedure)
*   [Confirming network links are established](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Confirmingnetworklinksareestablished)
*   [Discovering or configuring the platform management IP address](#Gettingstarted-Applicationinstallationandperformancebenchmarking-anchor_DiscoveringtheplatformmanagementIPaddressesDiscoveringorconfiguringtheplatformmanagementIPaddress)
    *   [Accessing the BIOS using a serial console (physical connection)](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AccessingtheBIOSusingaserialconsole(physicalconnection))
        *   [Prerequisites](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Prerequisites)
        *   [Port location](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Portlocation)
        *   [Access procedure](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Accessprocedure)
    *   [Accessing the BMC network configuration menu](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AccessingtheBMCnetworkconfigurationmenu)
    *   [Discovering the DHCP management IP address](#Gettingstarted-Applicationinstallationandperformancebenchmarking-DiscoDHCPIPBIOSDiscoveringtheDHCPmanagementIPaddress)
    *   [Configuring a static management IP address](#Gettingstarted-Applicationinstallationandperformancebenchmarking-ConfigStaticIPBIOSConfiguringastaticmanagementIPaddress)
*   [Preparing for operating system installation](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Preparingforoperatingsysteminstallation)
*   [Installing an operating system](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Installinganoperatingsystem)
    *   [Prerequisites](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Prerequisites.1)
    *   [Browser considerations](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Browserconsiderations)
    *   [Connecting to the Web UI of the BMC](#Gettingstarted-Applicationinstallationandperformancebenchmarking-ConnectingtotheWebUIoftheBMC)
    *   [Launching the KVM](#Gettingstarted-Applicationinstallationandperformancebenchmarking-LaunchingtheKVM)
    *   [Mounting the operating system image via virtual media](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Mountingtheoperatingsystemimageviavirtualmedia)
    *   [Accessing the BIOS setup menu](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AccessingtheBIOSsetupmenu)
    *   [Selecting the boot order from boot override](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Selectingthebootorderfrombootoverride)
    *   [Completing operating system installation](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Completingoperatingsysteminstallation)
    *   [(Optional) Changing the boot order in the BIOS menu](#Gettingstarted-Applicationinstallationandperformancebenchmarking-(Optional)ChangingthebootorderintheBIOSmenu)
*   [Verifying operating system installation](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Verifyingoperatingsysteminstallation)
*   [Benchmarking an application](#Gettingstarted-Applicationinstallationandperformancebenchmarking-Benchmarkinganapplication)
*   [Monitoring platform sensors using the Web UI](#Gettingstarted-Applicationinstallationandperformancebenchmarking-MonitoringplatformsensorsusingtheWebUI)
*   [Managing PCIe add-in card temperature for system cooling](#Gettingstarted-Applicationinstallationandperformancebenchmarking-MngtPCIeTempManagingPCIeadd-incardtemperatureforsystemcooling)
    *   [Accessing the Web UI](#Gettingstarted-Applicationinstallationandperformancebenchmarking-AccessingtheWebUI)
    *   [Configuring the PCIe add-in card temperature sensor thresholds](#Gettingstarted-Applicationinstallationandperformancebenchmarking-ConfiguringthePCIeadd-incardtemperaturesensorthresholds)

* * *

  

![](attachments/799310273/799211912.png)

Before working with this product or performing instructions described in the getting started section or in other sections, read the Safety and regulatory information section pertaining to the product. Assembly instructions in this documentation must be followed to ensure and maintain compliance with existing product certifications and approvals. Use only the described, regulated components specified in this documentation. Use of other products/components will void the CSA certification and other regulatory approvals of the product and will most likely result in non-compliance with product regulations in the region(s) in which the product is sold.

Introduction
============

This getting started section describes the network integration, platform access and operating system installation steps required to start operating an ME1100 platform equipped with one PCIe add-in card provided by the customer and 4 SSD drives and used to leverage two segregated network links (one for the management plane and one for the data plane).  

  

This use case is based on a simplified architecture with one management plane and one data plane.

![](attachments/786530314/864977047.png?width=650)

To visualize the complete platform architecture or for further details, refer to [Product architecture](Product-architecture_786530334.html).

Assumptions
-----------

The scenario described in this getting started section is based on the following assumptions:

*   The network connections of the system are as follows:
    *   One management plane (red line) via the RJ45 management port  
        
    *   One data plane (green line) via the SFP+ port 1  
        
    *   One serial connection via the RJ45 serial port to obtain or configure the BMC management IP address
*   The default IPv4 scheme is DHCP, but it can be configured as static in the BIOS setup menu  
    
*   The preferred method to obtain or configure the BMC management IP address is through a serial console (physical connection)
*   The preferred OS installation method is through the KVM
*   The preferred access method is through the Web UI
*   PCIe add-in card temperature is monitored using a thermal probe installed in the platform

Unboxing the platform
=====================

What's in the box
-----------------

The ME1100 platform box includes **one ME1100 edge computing 1U platform.**

__![](attachments/812286306/812025911.png?height=250)__

  

  

Step\_1

Carefully remove the platform from its packaging.

Step\_2

Remove the plastic film from the platform. **Failure to do so may affect platform airflow efficiency, thus resulting in poor cooling capabilities.**

**NOTE:** Additional material may be required to proceed with installation and configuration (refer to [Material and information required](#Gettingstarted-Applicationinstallationandperformancebenchmarking-anchor_Materialandinformationrequired) for more information).

  

Planning
========

Material and information required
---------------------------------

### PCIe add-in card

  

Item\_1

One T8 Torx screwdriver

Item\_2

One 3-mm flat-head screwdriver  

Item\_3

One T10 Torx screwdriver

Item\_4

One tie wrap, if the PCIe add-in card is a FH3/4L  

Item\_5

One thermal probe for temperature monitoring (if physical temperature monitoring is chosen)

### Power cables and tooling

  

Item\_1

Crimp lugs:

*   Two or four Molex insulated spade crimp lugs for 14-16 wire gauge (19131-0023)

**OR**

*   Two or four Panduit insulated ring crimp lugs for 10-12 wire gauge (EV10-6RB-Q)

Item\_2

Black stranded wire to build the power cable based on the length required:

*   14 AWG insulation diameter max.: 4.40 mm \[0.175 in\] for Molex crimp lugs

**OR**

*   12 AWG insulation diameter max.: 5.8 mm \[0.23 in\] for Panduit crimp lugs

Item\_3

Red stranded wire to build the power cable based on the length required:

*   14 AWG insulation diameter max.: 4.40 mm \[0.175 in\] for Molex crimp lug  
    

**OR**

*   12 AWG insulation diameter max.: 5.8 mm \[0.23 in\] for Panduit crimp lug

Item\_4

One hand crimp tool:

*   Molex Premium Grade Hand Crimp Tool (640010100)  
    

**OR**

*   Panduit Hand Crimp Tool (638130400)  
    

Item\_5

One 8 AWG ground cable based on the length required  

item\_6

One ground lug right angle, 8 AWG (Kontron P/N 1064-4226)

item\_7

One hand crimp tool, Panduit CT-1700 

Item\_8

8 mm wrench or equivalent tool

### Rack installation material

  

Item\_1

Racking fasteners (rack specific)

### Network cables and modules

**Relevant section:**

[Hardware compatibility list](Hardware-compatibility-list_786530374.html)

  

Item\_1

One SFP or SFP+ data plane module and cable  

*   SFP/SFP+ optical modules (SX, LX, SR, LR) with compatible optical cable

Item\_2

One RJ45 Ethernet management plane cable

Item\_3

One RJ45 serial connection cable

### Network infrastructure

*   Two IP addresses:
    *   One management plane IP  
        
    *   One data plane IP  
        

Software required
-----------------

  

Item\_1

A community version of **ipmitool** is installed on a remote computer to enable remote monitoring—it is recommended to use ipmitool version 1.8.18.

Item\_2

A terminal emulator such as **puTTY** is installed on a remote computer.

Item\_3

A hardware detection tool such as **pciutils** is installed on the local server to view information about devices connected to the server PCI buses.

  

  

\> You now have the material and software required. Proceed with the installation of the PCIe add-in card.

  

Installing a PCIe add-in card in an ME1100
==========================================

![](attachments/802456174/802685296.png?width=53)

**ESD sensitive device!**   
This equipment is sensitive to static electricity. Care must therefore be taken during all handling operations and inspections of this product in order to ensure product integrity at all times.

  

  

![](attachments/820412686/820379858.png)

This product usually has more than one power supply cord. Disconnect all power supply cords before servicing to avoid electric shock.

Opening the enclosure  

------------------------

  

Step\_1

Remove the 4 screws in the back using a T8 Torx screwdriver.

![](attachments/812060132/846626932.png?width=500)

Step\_2

Remove the 2 screws on top using a T8 Torx screwdriver.

Step\_3

On both sides of the unit, insert a flat-head screwdriver in the cavity shown, and slightly slide the cover to the back to release it.  

  

![](attachments/812060132/846626942.png?width=500)

Step\_4

Apply pressure on the cover with your hands to release it from the casing.

![](attachments/812060132/838336866.png?width=500)  

Step\_5

Make sure the EMI shield brackets are in their appropriate position. They should not fall in the chassis.

![](attachments/812060132/862847259.png?height=250)  

  

  

  

  

  

  

Adjusting the PCIe add-in card space length to three-quarter length
-------------------------------------------------------------------

The maximum form factor of the optional PCIe add-in card is full-height, three-quarter length (FH3/4L). T8 and T10 Torx screwdrivers, cutting pliers and a tie wrap are required.

**NOTE:** The unit's default configuration is set for a full-height, half-length (FHHL) PCIe add-in card. If your PCIe add-in card is FHHL, skip to [Connecting the PCIe add-in card](#Gettingstarted-Applicationinstallationandperformancebenchmarking-anchor_ConnectingthePCIeadd-incard).

Adjusting the PCIe add-in card rear mounting bracket
----------------------------------------------------

  

Step\_1

Cut the tie wrap holding the power cable. Be careful not to cut one of the wires.

  

![](attachments/817791003/832963238.png?height=250)

Step\_2

Remove the PCIe rear mounting bracket from the enclosure by removing the screw with a T8 Torx screwdriver.

  

![](attachments/817791003/817725495.png?width=528)

Step\_3

Move the PCIe rear mounting bracket to the desired position and fasten the screw with a T8 Torx screwdriver.

![](attachments/817791003/817725499.png?width=523)

Step\_4

Reattach the power cable using a tie wrap as shown in the picture.

![](attachments/817791003/832963238.png?height=250)

Connecting the PCIe add-in card
-------------------------------

  

Step\_1

Disconnect the PCIe riser by removing the 2 screws with a T8 Torx screwdriver.

![](attachments/818282607/843842449.png?width=500)

Step\_2

Install the PCIe add-in card onto the PCIe riser.  

Mount the front plate adapter onto the PCIe add-in card's L-bracket.  

Fasten the front plate adapter screw to the L-bracket using a T10 Torx screwdriver (6 lbf·in torque).  

![](attachments/818282607/845644241.png?width=500)

Step\_3

Remove the 2 mounting screws from the rear mounting bracket using a T8 Torx screwdriver.

Carefully insert the PCIe add-in card assembly into the unit by fastening the following 6 screws:

*   2 T8 Torx screws for the riser card onto the server motherboard (4 lbf·in torque)
*   2 T8 Torx screws for the PCIe add-in card into the rear mounting bracket (4 lbf·in torque)
*   2 T10 Torx captive screws into the front plate (6 lbf·in torque)

  

![](attachments/818282607/846332029.png?width=500)

![](attachments/818282607/846332034.png?width=500)

  

  

Installing a thermal probe for the PCIe add-in card
---------------------------------------------------

### Locating the thermal probe connection

![](attachments/786530314/863830045.png?width=750)

### Installing the thermal probe

**Relevant sections:**

[Managing customer added sensors](Managing-customer-added-sensors_851738646.html)

[Platform cooling and thermal management](Platform-cooling-and-thermal-management_804553390.html)

[Monitoring](Monitoring_786530502.html)

  

![](attachments/863666183/864026811.png?width=550)

Step\_1

Install the thermal probe in the connector as prescribed in the thermal probe specifications.

Step\_2

Affix the NTC thermistor to the PCIe card.

Typically, thermistors are installed between the fins of the PCIe card heatsink. Do not forget to use glue that can withstand the temperature.

**NOTE:** Configuration will be performed once the platform is operational (thresholds, specific software configurations, etc.).

  

  

  

Closing the enclosure
---------------------

  

Step\_1

Align the cover lock mechanisms with the cutouts on the chassis and slide it toward the front to fasten it into place.   

![](attachments/817594434/846397537.png?height=250)

![](attachments/817594434/838336888.png?width=450)

Step\_2

Insert the 4 T8 Torx screws in the back and the 2 T8 Torx screws on top without turning them, making sure the holes on the cover and the holes on the chassis are properly aligned.

**NOTE:** Tightening screws into unaligned holes will damage the threads.

![](attachments/817594434/846299232.png?width=450)

Step\_3

Tighten the 6 screws using a T8 Torx screwdriver to lock the cover in place.

  

  

  

Racking the platform
====================

The airflow of the platform goes from right to left, facing front. Ensure there is no physical obstruction that would hinder proper airflow when choosing a location for the platform in the rack.

  

Step\_1

Choose a location for the platform in the rack.

Step\_2

Insert the platform in the rack.

Step\_3

Fasten the platform to the rack using the appropriate fasteners.

![](attachments/832602179/853442618.png?width=333)

Step\_4

If a ground lug is installed, remove the 2 nuts and washers from the ground lug studs. Take out the ground lug.

![](attachments/832602179/853409838.png?width=333)  

  

Step\_5

Strip 19 mm (0.75 in) of the 8 AWG ground cable.

Step\_6

Insert the 8 AWG ground cable in the ground lug. Crimp the lug on the cable using an appropriate hand crimp tool (e.g. Panduit CT-1700 crimp tool set at: Color Code = Red; Die Index No. = P21).

![](attachments/832602179/832667675.png?height=250)

Step\_7

Install the ground lug on the studs, fastening with the 2 nuts and washers.

**NOTE:** The thread of the two chassis ground lugs is M5x0.8. 

![](attachments/832602179/853409838.png?width=333)

  

  

\> You are now ready to connect the network and power cables and start platform configuration.

Connecting the network cables
=============================

Connect the network cables according to the image below:

1.  Connect one RJ45 cable to port 3 for the management plane.
2.  Connect one SFP or SFP+ cable to port 1 for the data plane.

  

![](attachments/786530314/841155761.jpg?width=700)

Building and connecting the power cables
========================================

![](attachments/799342966/799211997.png)

Installation of this product must be performed in accordance with national wiring codes and conform to local regulations.

  

Material required
-----------------

Kontron suggests using crimp lugs (ring or spade crimp lug, straight, isolated, UL94V-0) on the power cables. Connect the appropriate cable to the appropriate polarity.

Kontron suggests the following wire gauges for -48V DC and RTN: 14 AWG or 12 AWG.

Description

Quantity

Manufacturer P/N

Link

Crimp lugs:

*   Molex insulated spade crimp lugs for 14-16 wire gauge
*   Panduit insulated ring crimp lugs for 10-12 wire gauge

2 (or 4 for redundancy)

19131-0023

or equivalent

*   [Molex product catalog](https://www.molex.com/molex/products/datasheet.jsp?part=active/0191310023_RING_AND_SPADE_TER.xml)
*   [Part details](https://www.molex.com/webdocs/datasheets/pdf/en-us/0191310023_RING_AND_SPADE_TER.pdf)

EV10-6RB-Q

or equivalent

*   [Panduit product catalog](https://www.panduit.com/en/search.html?q=EV10-6RB-Q)
*   [Part drawing](http://www1.panduit.com/heiler/PartDrawings/C-TMEV10-6RB-Q--ENG.pdf)

Black stranded wire to build the power cable based on the length required:

*   14 AWG insulation diameter max.: 4.40 mm \[0.175 in\] for Molex crimp lugs
*   12 AWG insulation diameter max.: 5.8 mm \[0.23 in\] for Panduit crimp lugs

Length required

  

  

Red stranded wire to build the power cable based on the length required:

*   14 AWG insulation diameter max.: 4.40 mm \[0.175 in\] for Molex crimp lug  
    
*   12 AWG insulation diameter max.: 5.8 mm \[0.23 in\] for Panduit crimp lug  
    

Length required

  

  

Hand crimp tool:

*   Molex Premium Grade Hand Crimp Tool
*   Panduit Hand Crimp Tool

  

1  
  

640010100

or equivalent

*   [Molex product catalog](https://www.molex.com/molex/search/partSearch?query=64001-0100&pQuery=)
*   [Application tooling specification sheet](https://www.molex.com/pdm_docs/ats/ATS-640010100.pdf)

CT-460

or equivalent

*   [Panduit product catalog](https://www.panduit.com/en/search.html?q=ct-460)
*   [Application tooling specification sheet](https://www.panduit.com/content/dam/panduit/en/products/media/0/20/320/7320/106727320.pdf)

Procedure
---------

  

Step\_1

Strip 6 mm \[0.236 in\] from the end of a black stranded 14 AWG wire (for Molex crimp lug 19131-0023) or 8 mm \[0.315 in\] from the end of a black stranded 12 AWG wire (for Panduit crimp lug EV10-6RB-Q).

Step\_2

Strip 6 mm \[0.236 in\] from the end of a red stranded 14 AWG wire (for Molex crimp lug 19131-0023) or 8 mm \[0.315 in\] from the end of a red stranded 12 AWG wire (for Panduit crimp lug EV10-6RB-Q).

Step\_3

Insert each wire in a crimp lug. Follow the crimp lug manufacturer's procedure, using the appropriate hand crimp tool as specified in the Application tooling specification sheet of the tool.

Step\_4

Bend the crimp lugs to a 45° angle as shown in the image.

![](attachments/838238330/838336703.png?width=360)

Step\_5

Remove the screw from the terminal block RTN "B" location.

Step\_6

Insert the crimped red wire in the RTN "B" location as shown in the image.

Step\_7

Screw the crimp lug in place.

Step\_8

Remove the screw from the terminal block -48V DC "B" location.

Step\_9

Insert the crimped black wire in the -48V DC "B" location as shown in the image.

Step\_10

Screw the crimp lug in place.

Step\_11

(Optional) If redundancy is required, repeat steps 1 to 10 for a second set of cables. They are to be installed in the -48V DC and RTN "A" locations.

Step\_12

Put the plastic terminal cover back in place once all the cables are screwed in place.

**NOTE:** The power supply is reverse polarity protected. The unit will power on as soon as external power is applied (green power LED). 

  

  

![](attachments/786530314/832602142.png?width=46)

Generally, the platform fans are not turned on when components are not above the temperature thresholds set. Therefore, it may be normal not to hear the fans when the system is powered up in an environment where the ambient temperature is within a normal range.

Refer to [Platform cooling and thermal management](Platform-cooling-and-thermal-management_804553390.html) for more information about fan behavior over the entire operating temperature range.

  

\> You are now ready to start platform configuration.

Confirming network links are established
========================================

Once the ME1100 power LED is **green ON** (normal blink or ON), confirm LAN connection with the management plane and data plane:

*   The right LED on Port 3 (management) should be **green ON**
*   The LED on SFP or SFP+ Port 1 should be **green ON**

Refer to [Platform components](Platform-components_786530330.html) for more information about LED location and behavior.

If LED behavior is not as expected, refer to your IT personnel to review upstream network status (the top-of-rack switch port might be disabled).

  

Discovering or configuring the platform management IP address
=============================================================

The platform management IP address is the minimum required to access the Web UI, the monitoring interface and the KVM to install an operating system.

The IP address can be discovered or configured using the BIOS menu. When no OS is installed and the IP address is not known, the BIOS must be accessed via a serial console (physical connection). 

For detailed information on MAC addresses and IPs, refer to [Baseboard management controller (BMC)](786530462.html) and [MAC addresses](MAC-addresses_841384530.html).

  

Accessing the BIOS using a serial console (physical connection)
---------------------------------------------------------------

### Prerequisites

  

1

A physical connection to the device is required.

**NOTE:** The serial console port is compatible with Cisco 72-3383-01 cable.

2

A serial console tool is installed on the external computer.  

*   Speed (Baud) : 115200
*   Data bits : 8
*   Stop bits : 1
*   Parity : None
*   Flow Control : None
*   Recommended emulation mode : VT100+

**NOTE:**  PuTTY is recommended.

### Port location

![](attachments/786530314/859209760.png?height=250)

### Access procedure

  

Step\_1

From a computer with a physical connection to the serial port, open a serial console tool and start the communication between the console and the port to which the device is connected.

Step\_2

Perform a server reset (**Ctrl-break** hot key).  

**NOTE: **If an operating system is installed on the device, the hot key might not work properly. If this is the case, reset the server as recommended for the operating system.

**NOTE:** When a server reset command is sent, it may take a few seconds for the BIOS sign on screen to display.

![](attachments/838370459/838305455.png?width=450)

Step\_3

When the BIOS sign on screen is displayed, press the specified key to enter the BIOS setup menu.

**NOTE:**It may take a few seconds for the BIOS sign on screen to display confirmation message “Entering Setup…”.

![](attachments/838370459/841875637.png?width=450)

Step\_4

TheBIOS sign on screen displays“Entering Setup…”.

**NOTE:**It will take several seconds to display and enter the BIOS setup menu.

![](attachments/838370459/841843062.png?width=450)

Step\_5

The BIOS setup menu is displayed.

![](attachments/838370459/838305467.png?width=450)

  

  

  

Accessing the BMC network configuration menu
--------------------------------------------

Step\_1

From the BIOS menu, use the arrow keys to select **Server Mgmt**.

![](attachments/838370463/838305511.png?width=450)  

  

Step\_2

Use the arrow keys to select **BMC network configuration**.

![](attachments/838370463/838305515.png?width=450)

Step\_3

The **BMC network configuration** menu is displayed.

**NOTE:** When the platform is powered up after being shut off, the BIOS may load before the BMC has received its IP address. In this case, the BIOS menu information will need to be refreshed by restarting the server and re-entering the BIOS.

![](attachments/838370463/838305519.png?width=450)

From the BMC network configuration menu, you have two options according to your network scheme:

*   To discover the DHCP IP address, go to section [DHCP](#Gettingstarted-Applicationinstallationandperformancebenchmarking-DiscoDHCPIPBIOS)
*   To configure a static IP address, go to section [static](#Gettingstarted-Applicationinstallationandperformancebenchmarking-ConfigStaticIPBIOS)

In this use case, only BMC LAN channel 1 will be configured as the management plane is connected through port 3. Refer to [Product architecture](Product-architecture_786530334.html) for a visual representation and information on network connectivity.

Discovering the DHCP management IP address
------------------------------------------

  

Step\_1

From the **BMC network configuration** menu.

If an IP address is displayed, make a note of it as it is your management IP address (BMC MNGMT\_IP).

**OR**

If the IP address displayed is 0.0.0.0, perform optional steps 2, 3 and 4.

**NOTE:** When the platform is powered up after being shut off, the BIOS may load before the BMC has received its IP address via DHCP. In this case, the BIOS menu information will need to be refreshed (steps 2 to 4).

![](attachments/862486535/862389503.png?width=450)

Step\_2

(Optional) Navigate to **Save & Exit.**

![](attachments/862486535/862389507.png?width=420)

Step\_3

(Optional) Select **Save Changes and Exit** or **Discard Changes and Exit**, this will perform a server reset.

![](attachments/862486535/862389511.png?height=250)

Step\_4

(Optional) When the BIOS sign on screen is displayed, press the specified key to enter the BIOS setup menu. Then, access the **Server Mgmt** menu and select **BMC network configuration**. Make a note of the address displayed as it is your management IP address (BMC MNGMT\_IP).

Configuring a static management IP address
------------------------------------------

**NOTE:** If you are in a DHCP network, skip this section.

  

Step\_1

From the **BMC network configuration** menu, select the **Configuration Address source** option for the LAN interface to configure (LAN channel 1 in this example).

![](attachments/861570810/862486778.png?width=450)

Step\_2

Select **Static.**

![](attachments/861570810/862486782.png?width=350)

Step\_3

Change the **Station IP address**.

**NOTE:** This is the management IP address (**BMC MNGMT\_IP**).

![](attachments/861570810/862486786.png?width=350)

Step\_4

Change the **Subnet mask**.

![](attachments/861570810/862486790.png?width=350)

Step\_5

(Optional) Change the **Router IP address**.

![](attachments/861570810/862486794.png?width=350)

Step\_6

Confirm the configuration has changed and exit **BMC network configuration** using the **ESC** key.

![](attachments/861570810/862486798.png?width=350)

\> With your management IP (BMC MNGMT\_IP), you are now ready to start the OS installation.

Preparing for operating system installation
===========================================

  

Step\_1

Choose the operating system needed based on the requirements of your application (CentOS 7.4 or latest version is recommended).

Step\_2

Confirm the OS version to be installed includes or is compatible with the following network interface drivers: **igb **and **ixgbe**.  

Step\_3

If applicable, download the ISO file of the OS to be installed.

For a list of known compatible operating systems, refer to [Validated operating systems](Validated-operating-systems_786530382.html).

For information on components, refer to the [PCI mapping](PCI-mapping_840139212.html).

Installing an operating system
==============================

Prerequisites
-------------

  

1

The BMC IP address is known (refer to section Configuring/Baseboard management controller (BMC) to obtain the BMC MNGMT\_IP).

2

The remote computer has access to the management network subnet.

Browser considerations
----------------------

  

**HTML5**

To connect to the Web UI, a Web browser supporting HTML5 is required. 

**HTTPS self-signed certificate**

Upon connection to the Web UI, it is mandatory to accept the HTTPS self-signed certificate. For further information about accepting HTTPS self-signed certificates, please refer to your Web browser's documentation.

**File download permission**

File download from the site needs to be permitted. For further information about file download permission, please refer to your Web browser's documentation.

**Cookies**

Cookies must be enabled in order to access the website. For further information about enabling cookies, please refer to your Web browser's documentation.

**NOTE:** The procedure may vary depending on the browser used. Examples provided use Firefox.

Connecting to the Web UI of the BMC  

--------------------------------------

  

Step\_1

From a remote computer that has access to the management network, open a browser window and enter the IP address discovered for the BMC. 

**NOTE: The HTTPS prefix is mandatory.**

_https://\[BMC MNGMT\_IP\] _

Step\_2

Click on **Advanced** in order to start the HTTPS self-signed certificate acceptance process. Information on the error message will be displayed.

![](attachments/806486321/806551873.png?width=450)

Step\_3

Click on **Add Exception…** The Add Security Exception pop-up window will be displayed. Click on **Confirm Security Exception** to allow the browser to access the management Web UI of this interface.

![](attachments/806486321/806551877.png?width=450)

Step\_4

Log in to the BMC Web UI using the appropriate credentials.

**NOTE:** Default Web UI user name and password is admin/admin.  

![](attachments/806486321/833127743.png?height=400)

Step\_5

You now have access to the management Web UI of the BMC. You can use the interface.

![](attachments/806486321/833127749.png?height=250)

Launching the KVM
-----------------

The Web UI allows remote control of the server through a KVM (Keyboard, Video, Mouse) interface. 

Step\_1

From the left menu, click on**Remote Control**.

![](attachments/806486374/833257792.png?height=250)

Step\_2

From the**Remote Control** menu, click on the**Launch KVM**button. 

![](attachments/806486374/833323535.png?height=250)

Step\_3

A new browser window opens and displays the server screen.  
**NOTE:** If an OS is installed, the image displayed might be that of the OS.

![](attachments/806486374/806584596.png?width=475)

  

Mounting the operating system image via virtual media
-----------------------------------------------------

Step\_1

From the KVM view of the server screen, click on **Browse File** at the top right of the screen. Select the ISO file to be mounted and click on **Open**.

![](attachments/847741169/847741215.png?width=500)

Step\_2

Once the ISO file is loaded, click on **Start Media** at the top right of the screen.

**NOTE:** Once clicked, the Start Media button becomes the **Stop Media** button.

![](attachments/847741169/847741219.png?width=500)

  

![](attachments/847741169/847741223.png?width=500)

Accessing the BIOS setup menu
-----------------------------

Step\_1

From the **Power** drop-down menu, select **Reset Server** to access the BIOS menu. Click on **OK** to confirm the operation.

**NOTE:** When a reset server command is launched, it may take a few seconds for the BIOS sign on screen to display.

![](attachments/806420808/806420812.png?width=475)

![](attachments/806420808/806420816.png?width=475)

Step\_2

When the BIOS sign on screen is displayed, press the specified key to enter the BIOS setup menu.

**NOTE:** It may take a few seconds for the BIOS sign on screen to display confirmation message “Entering Setup…”.

![](attachments/806420808/806420820.png?width=475)

Step\_3

The BIOS sign on screen displays “Entering Setup…”.

**NOTE:** It will take several seconds to display and enter the BIOS setup menu.

![](attachments/806420808/806420824.png?width=475)

Step\_4

The BIOS setup menu will be displayed.

![](attachments/806420808/806420828.png?width=475)

Selecting the boot order from boot override
-------------------------------------------

Step\_1

From the BIOS setup menu and using the keyboard arrows, select the **Save & Exit** menu. In the **Boot Override** section, select **UEFI: AMI Virtual CDROM0 1.00** and press **Enter**. The server will reboot and the media installation process will start.

![](attachments/847741169/847741227.png?width=500)

  

\> You are now ready to complete operating system installation according to your application requirements.

Completing operating system installation
----------------------------------------

Step\_1

Complete the installation by following the on-screen prompts of the specific OS installed.

(Optional) Changing the boot order in the BIOS menu
---------------------------------------------------

  

![](attachments/786530314/832602142.png?width=46)

After installation, if booting from network (PXE) occurs and is not desired, your operating system installer may not have modified the BIOS boot order. To correct this, enter BIOS setup again and follow the steps below.

  

  

Step\_1

From the BIOS setup menu, use the keyboard arrows to select the **Boot** menu. Configure the boot order as desired.

![](attachments/847348344/847643081.png?width=500)

Step\_2

Using the keyboard arrows, select the **Save & Exit** menu, go to **Save Changes and Exit** and press **Enter** to confirm and save the new boot order.

![](attachments/847348344/847643085.png?width=500)

Verifying operating system installation
=======================================

  

![](attachments/847904911/862355847.png?width=46)

All the results and commands may vary depending on the operating system and the devices added.

  

Step\_1

Reboot the OS as recommended, then access the OS command prompt.

Step\_2

Verify that no error messages or warnings are displayed in **dmesg** using the following commands. 

LocalServer\_OSPrompt:~#** dmesg | grep -i fail**

LocalServer\_OSPrompt:~# **dmesg | grep -i Error**

LocalServer\_OSPrompt:~# **dmesg | grep -i Warning**

LocalServer\_OSPrompt:~# **dmesg | grep -i “Call trace”**

**NOTE: **If there are any messages or warnings displayed, refer to the operating system's documentation to fix them.

Step\_3

Verify that the DIMMs are detected.

LocalServer\_OSPrompt:~# **free -h**

![](attachments/847904911/847413643.png?width=500)

Step\_4

Verify that all the storage devices are detected.

LocalServer\_OSPrompt:~# **lsblk**

![](attachments/847904911/847413647.png?width=350)

Step\_5

Confirm the control plane network interface controller is loaded by the **igb **driver.

LocalServer\_OSPrompt:~# **dmesg | grep igb**

  

**NOTE: **You should discover one 1GbE NIC.

![](attachments/847904911/867041484.png?width=600)

Step\_6

Confirm the data plane network interface controllers are loaded by the **ixgbe**driver.

LocalServer\_OSPrompt:~# **dmesg | grep ixgbe  
**

**NOTE:** You should discover two 10GbE NIC.

![](attachments/847904911/847413655.png?width=600)

Step\_7

Confirm that all the network interfaces are detected.

LocalServer\_OSPrompt:~# **ip address**

  

**NOTE: **You should discover one 1GbE NIC and two 10GbE NIC.

![](attachments/847904911/867041540.png?width=600)

Step\_8

Configure network interface controllers based on your requirements.

  

**NOTE: **Interface names may change depending on the OS installed. However, parameters Bus:Device.Function stay the same for the interface regardless of the operating system.

![](attachments/847904911/865076727.png?width=600)

Step\_9

Install **ipmitool** and **pciutils** using the package manager, and update the operating system packages. The ipmitool version recommended is 1.8.18.

Example:

LocalServer\_OSPrompt:~# **yum update**

LocalServer\_OSPrompt:~# **yum install ipmitool**

LocalServer\_OSPrompt:~# **yum install pciutils**

  

**NOTE: **Updating the packages may take a few minutes.

Step\_10

(Optional) If a PCIe add-in card is installed, verify that the card is detected.

LocalServer\_OSPrompt:~# **lspci**

![](attachments/847904911/847413667.png?width=600)

Step\_11

Verify communication between the operating system and the BMC.

LocalServer\_OSPrompt:~# **ipmitool mc info**

![](attachments/847904911/847413671.png?width=300)

Benchmarking an application
===========================

Install your application and proceed with benchmarking.

Monitoring platform sensors using the Web UI
============================================

**NOTE: **Refer to [Accessing a BMC on an ME1100](Accessing-a-BMC-on-an-ME1100_808681475.html) to access the BMC Web UI.

The key sensors to look at are the following:

*   Temperature sensors
*   Power sensors

  

Step\_1

Access the BMC Web UI.

Step\_2

From the left-side menu, click on **Sensor. **

![](attachments/847741077/847642749.png?height=250)

Step\_3

The sensor list will be displayed.

![](attachments/847741077/847642753.png?width=540)

Step\_4

Scroll down to see the list of sensors

![](attachments/847741077/868712863.png?width=540)

Step\_5

Click on a sensor in order to see more details.

![](attachments/847741077/847642757.png?width=540)

For a list of all the sensors, refer to [Sensor list](Sensor-list_826737000.html).

For more monitoring methods refer to [Monitoring](Monitoring_786530502.html).

Managing PCIe add-in card temperature for system cooling
========================================================

**Relevant sections:**

[Platform cooling and thermal management](Platform-cooling-and-thermal-management_804553390.html)

[Managing customer added sensors](Managing-customer-added-sensors_851738646.html)

[Monitoring](Monitoring_786530502.html)

  

Accessing the Web UI
--------------------

**NOTE: **Refer to [Accessing a BMC on an ME1100](Accessing-a-BMC-on-an-ME1100_808681475.html) to access the BMC Web UI.

  

Step\_1

Access the BMC Web UI.

Step\_2

From the left-side menu, click on **Sensor. **

![](attachments/847741077/847642749.png?height=250)

Step\_3

The sensor list will be displayed.

![](attachments/847741077/847642753.png?width=540)

Step\_4

Scroll down to see the list of sensors

![](attachments/847741077/868712863.png?width=540)

Step\_5

Click on a sensor in order to see more details.

![](attachments/847741077/847642757.png?width=540)

  

Configuring the PCIe add-in card temperature sensor thresholds
--------------------------------------------------------------

The **Temp NTC** sensor is the one linked to the thermal probe physically connected in the platform. Its thresholds need to be set according to the specific parameters and uses of the PCIe add-in card installed. Proceed as shown below with the appropriate sensor (Temp NTC).  

  

Step\_1

From the sensor detail page, click on **Change Thresholds**.

![](attachments/864026911/869762675.png?height=250)

Step\_2

Set the thresholds as desired and click on **S****ave**.

![](attachments/864026911/863634140.png?width=300)

  

Attachments:
------------

![](images/icons/bullet_blue.gif) [image2019-4-17\_8-58-29.png](attachments/786530314/802095137.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-4-17\_7-45-46.png](attachments/786530314/802095141.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-7\_13-57-27.png](attachments/786530314/802095145.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-21\_11-26-41.png](attachments/786530314/802095149.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-21\_11-27-5.png](attachments/786530314/802095153.png) (image/png)  
![](images/icons/bullet_blue.gif) [login\_prompt.png](attachments/786530314/802095157.png) (image/png)  
![](images/icons/bullet_blue.gif) [remote\_control.png](attachments/786530314/802095161.png) (image/png)  
![](images/icons/bullet_blue.gif) [launch\_kvm.png](attachments/786530314/802095165.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-22\_11-44-59.png](attachments/786530314/802095169.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-20\_16-33-18.png](attachments/786530314/802095173.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-20\_16-33-54.png](attachments/786530314/802095177.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-20\_16-37-14.png](attachments/786530314/802095181.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-28\_11-30-30.png](attachments/786530314/802095185.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-28\_11-38-15.png](attachments/786530314/802095189.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-22\_11-46-33.png](attachments/786530314/802095193.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-20\_16-47-57.png](attachments/786530314/802095197.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-22\_12-6-3.png](attachments/786530314/802095201.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-22\_11-47-54.png](attachments/786530314/802095205.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-22\_14-49-33.png](attachments/786530314/802095209.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-21\_17-5-30.png](attachments/786530314/802095213.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-21\_17-4-56.png](attachments/786530314/802095217.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-4-1\_17-19-26.png](attachments/786530314/802095221.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-4-3\_9-42-41.png](attachments/786530314/802095225.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-4-3\_9-45-47.png](attachments/786530314/802095229.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-4-1\_17-25-21.png](attachments/786530314/802095233.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-15\_11-58-46.png](attachments/786530314/806585033.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2018-12-20\_13-51-42.png](attachments/786530314/806486836.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-22\_11-54-17.png](attachments/786530314/809238706.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-22\_11-57-46.png](attachments/786530314/809271508.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-22\_11-58-8.png](attachments/786530314/809238714.png) (image/png)  
![](images/icons/bullet_blue.gif) [17C107102.stp](attachments/786530314/810256625.stp) (application/octet-stream)  
![](images/icons/bullet_blue.gif) [image2019-5-27\_9-25-39.png](attachments/786530314/812058472.png) (image/png)  
![](images/icons/bullet_blue.gif) [worddavbe262a3b52c413354315045d42e036fb.png](attachments/786530314/817692747.png) (image/png)  
![](images/icons/bullet_blue.gif) [worddav3c07334bb86be5bc7211913aaf16892f.png](attachments/786530314/817692751.png) (image/png)  
![](images/icons/bullet_blue.gif) [Ground\_Lug.png](attachments/786530314/819101719.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-27\_8-58-14.png](attachments/786530314/819593224.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-27\_9-11-59.png](attachments/786530314/819331076.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-5-27\_9-23-26.png](attachments/786530314/819527685.png) (image/png)  
![](images/icons/bullet_blue.gif) [Getting-started-network.png](attachments/786530314/824049835.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-28\_16-9-30.png](attachments/786530314/832602119.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-3-28\_11-35-7.png](attachments/786530314/832602123.png) (image/png)  
![](images/icons/bullet_blue.gif) [worddav812f908d827be59c1efb56931fcd1010.png](attachments/786530314/832602142.png) (image/png)  
![](images/icons/bullet_blue.gif) [ME2400-Rev0-00-Rack-20181210.jpg](attachments/786530314/832569362.jpg) (image/jpeg)  
![](images/icons/bullet_blue.gif) [image2019-3-28\_13-22-36.png](attachments/786530314/832569366.png) (image/png)  
![](images/icons/bullet_blue.gif) [mcinfo.png](attachments/786530314/833618037.png) (image/png)  
![](images/icons/bullet_blue.gif) [mcinfo.png](attachments/786530314/833324346.png) (image/png)  
![](images/icons/bullet_blue.gif) [ping.png](attachments/786530314/833650881.png) (image/png)  
![](images/icons/bullet_blue.gif) [ping.png](attachments/786530314/833618048.png) (image/png)  
![](images/icons/bullet_blue.gif) [Login PAge.png](attachments/786530314/833683622.png) (image/png)  
![](images/icons/bullet_blue.gif) [igb.png](attachments/786530314/833782477.png) (image/png)  
![](images/icons/bullet_blue.gif) [igb.png](attachments/786530314/833325354.png) (image/png)  
![](images/icons/bullet_blue.gif) [igb.png](attachments/786530314/833325346.png) (image/png)  
![](images/icons/bullet_blue.gif) [ixgbe.png](attachments/786530314/833782502.png) (image/png)  
![](images/icons/bullet_blue.gif) [ixgbe.png](attachments/786530314/833749723.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-7-11\_15-6-31.png](attachments/786530314/835092591.png) (image/png)  
![](images/icons/bullet_blue.gif) [KCS.png](attachments/786530314/837255207.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS Setup Menu.png](attachments/786530314/837386359.png) (image/png)  
![](images/icons/bullet_blue.gif) [BMC\_ServerMgmt.png](attachments/786530314/837386363.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Network.png](attachments/786530314/837386367.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Verify1.png](attachments/786530314/837386371.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Verify2.png](attachments/786530314/837386375.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Serial\_Reboot.png](attachments/786530314/837419157.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Serial\_Press\_Del.png](attachments/786530314/837419161.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Serial\_Entering\_Setup.png](attachments/786530314/837419165.png) (image/png)  
![](images/icons/bullet_blue.gif) [BIOS\_Serial\_Screen.png](attachments/786530314/837419169.png) (image/png)  
![](images/icons/bullet_blue.gif) [CP0284.jpg](attachments/786530314/841188129.jpg) (image/jpeg)  
![](images/icons/bullet_blue.gif) [CP0285.jpg](attachments/786530314/841155761.jpg) (image/jpeg)  
![](images/icons/bullet_blue.gif) [ME1100v2\_Front\_View.png](attachments/786530314/841090374.png) (image/png)  
![](images/icons/bullet_blue.gif) [ME1100 product architecture.png](attachments/786530314/846561492.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-8-16\_13-55-30.png](attachments/786530314/854623061.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-8-16\_14-16-42.png](attachments/786530314/854688091.png) (image/png)  
![](images/icons/bullet_blue.gif) [image2019-8-15\_15-26-4.png](attachments/786530314/859209760.png) (image/png)  
![](images/icons/bullet_blue.gif) [ME1100 Top View\_Thermocouple location.png](attachments/786530314/872055296.png) (image/png)  
![](images/icons/bullet_blue.gif) [me1100-ug-getstarted-appinstall.png](attachments/786530314/865271876.png) (image/png)  
![](images/icons/bullet_blue.gif) [me1100-ug-getstarted-appinstall.png](attachments/786530314/864977047.png) (image/png)  
![](images/icons/bullet_blue.gif) [ME1100 Top View\_Thermocouple location.png](attachments/786530314/863830045.png) (image/png)  

Document generated by Confluence on Oct 17, 2019 08:00

[Atlassian](http://www.atlassian.com/)